from GalloisField import gf

def string2Byte(data):

    result = []
    for i in range(len(data)):
        result.append(bin(ord(data[i])))

    return result

gf1 = gf(0b10001001)
gf2 = gf(0b00101010)
gfi = gf(0b1)

print(gf1 * gf2)
print(gf1.inverse())
print(gfi / gf1)