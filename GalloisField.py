class gf:

    def __init__(self, val):
        self.__val = val
        initMulTbl()
    
    def __add__(self, gf2):
        return bin(self.__val ^ gf2.__val)

    def __sub__(self, gf2):
        return bin(self.__val ^ gf2.__val)

    def __mul__(self, gf2):
        if(self.__val == 0 or gf2.__val == 0):
            return 0
        return bin(gf_exp[gf_log[self.__val] + gf_log[gf2.__val]])

    def __truediv__(self, gf2):
        if gf2.__val==0:
            raise ZeroDivisionError()
        if self.__val==0:
            return 0
        return bin(gf_exp[(gf_log[self.__val] + 255 - gf_log[gf2.__val]) % 255])

    def __pow__(self, power):
        return bin(gf_exp[(gf_log[self.__val] * power) % 255])

    def inverse(self):
        return bin(gf_exp[255 - gf_log[self.__val]])

    def show(self):
        print(bin(self.__val))


def gf_mult_noLUT(x, y, prim=0, field_charac_full=256, carryless=True):
    
        r = 0
        while y:
            if y & 1: r = r ^ x if carryless else r + x
            y = y >> 1
            x = x << 1
            if prim > 0 and x & field_charac_full: x = x ^ prim

        return r

def initMulTbl(prim=0x11d):

    global gf_exp, gf_log
    gf_exp = [0] * 512
    gf_log = [0] * 256

    x = 1
    for i in range(0, 255):
        gf_exp[i] = x
        gf_log[x] = i
        x = gf_mult_noLUT(x, 2, prim)
        
    for i in range(255, 512):
        gf_exp[i] = gf_exp[i - 255]

    return [gf_log, gf_exp]